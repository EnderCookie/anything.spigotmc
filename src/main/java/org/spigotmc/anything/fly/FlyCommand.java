package org.spigotmc.anything.fly;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by insou on 21/12/2015.
 */
public class FlyCommand implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "You must be a Player to use this command!");
            return false;
        }
        if (!sender.hasPermission("anything.fly")) {
            sender.sendMessage(ChatColor.RED + "You don't have permission to do this!");
            return false;
        }
        Player player = (Player) sender;
        boolean flying = player.getAllowFlight();
        player.setAllowFlight(!flying);
        player.setFallDistance(0.0F);
        player.sendMessage(ChatColor.AQUA + "Flight " + (flying ? "dis" : "en") + "abled!");
        return false;
    }

}
