package org.spigotmc.anything.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class ItemUtils {

	/**
	 * This is a general utility that can be used to parse ItemStack strings back into item stacks. 
	 * This is preferred over using the ConfigurationSerializable methods as it can be used on more human readable strings. 
	 * It could also be useful for parsing user configurable items from strings in .yml or MySQL storage.
	 * 
	 * @param s
	 *            The string containing the serialised item stack.
	 * @return The parsed ItemStack of the provided string
	 */
	@SuppressWarnings("deprecation")
	public ItemStack parseItemStack(String s) {
		try {
			String[] gSplit = s.split(" ");
			ItemStack is = null;

			// ITEM ID / MATERIAL / SUBID
			String[] idsSplit = gSplit[0].split(":");
			try {
				is = new ItemStack(Integer.parseInt(idsSplit[0])); /* Only here in order to allow people that refuse to use the better Material Enum to continue to use magic values. */
			} catch (NumberFormatException e) {
				is = new ItemStack(Material.valueOf(idsSplit[0]));
			}

			if (idsSplit.length > 1)
				is.setDurability(Short.parseShort(idsSplit[1]));

			if (gSplit.length > 1) {
				int metaStart = 2;

				try {
					is.setAmount(Integer.parseInt(gSplit[1]));
				} catch (NumberFormatException e) {
					metaStart = 1;
				}
				ItemMeta im = is.getItemMeta();
				for (int meta = metaStart; meta < gSplit.length; meta++) {
					String rawKey = gSplit[meta];
					String[] split = rawKey.split(":");
					String key = split[0];

					if (key.equalsIgnoreCase("name")) {
						im.setDisplayName(ChatColor.translateAlternateColorCodes('&', split[1]).replace("_", " "));
					}

					else if (key.equalsIgnoreCase("lore")) {
						List<String> lore = new ArrayList<>();
						for (String line : split[1].split("//")) {
							lore.add(ChatColor.translateAlternateColorCodes('&', line).replace("_", " "));
						}
						im.setLore(lore);
					}

					else if (key.equalsIgnoreCase("color") && im instanceof LeatherArmorMeta) {
						LeatherArmorMeta lam = (LeatherArmorMeta) im;
						String[] csplit = split[1].split(",");
						Color color = Color.fromRGB(Integer.parseInt(csplit[0]), Integer.parseInt(csplit[1]), Integer.parseInt(csplit[2]));
						lam.setColor(color);
					}

					else if (key.equalsIgnoreCase("effect") && im instanceof PotionMeta) {
						PotionMeta pm = (PotionMeta) im;
						String[] psplit = split[1].split(",");
						pm.addCustomEffect(new PotionEffect(PotionEffectType.getByName(psplit[0]), Integer.parseInt(psplit[1]) * 20, Integer.parseInt(psplit[2])), true);
					}

					else if (key.equalsIgnoreCase("player") && im instanceof SkullMeta) {
						((SkullMeta) im).setOwner(split[1]);
					}

					else if (key.equalsIgnoreCase("enchant")) {
						String[] esplit = split[1].split(",");
						im.addEnchant(getEnchantmentType(esplit[0]), Integer.parseInt(esplit[1]), true);
					}

				}
				is.setItemMeta(im);
			}

			return is;
		} catch (Exception e) {
			System.err.println("\u001B[30;1m[\u001B[32;1manything.spigotmc\u001B[30;1m] \u001B[31;1mCannot parse ItemStack: " + s + " - Mabye this is the reason: " + e.toString() + "\u001B[0m");
			return null;
		}
	}

	/**
	 * This method allows for ItemStacks to be serialised into a human readable format that can be stored in something such as a .yml 
	 * file and be parsed back into an ItemStack at a later data.
	 * 
	 * @param is
	 *            The ItemStack provided for serialisation.
	 * @return A string containing the serialised item stack.
	 */
	public String serialiseItemStack(ItemStack is) {
		StringBuilder sb = new StringBuilder();
		try {
			sb.append(is.getType().name());
			if (is.getDurability() != 0) {
				sb.append(":" + is.getDurability());
			}

			if (is.hasItemMeta()) {
				ItemMeta im = is.getItemMeta();
				if (im.hasDisplayName()) {
					sb.append(" name:" + im.getDisplayName().replace('\u00A7', '&').replace(' ', '_'));
				}

				if (im.hasLore()) {
					sb.append(" lore:");
					Iterator<String> iter = im.getLore().iterator();
					sb.append(iter.next());
					while (iter.hasNext()) {
						sb.append("//");
						sb.append(iter.next());
					}
				}

				if (im.hasEnchants()) {
					for (Enchantment ench : im.getEnchants().keySet()) {
						sb.append(" enchant:" + serialiseEnchantmentType(ench) + "," + im.getEnchantLevel(ench));
					}
				}

				if (im instanceof LeatherArmorMeta) {
					LeatherArmorMeta lam = (LeatherArmorMeta) im;
					sb.append("color:" + lam.getColor().getRed() + "," + lam.getColor().getGreen() + "," + lam.getColor().getBlue());
				}

				else if (im instanceof PotionMeta) {
					PotionMeta pm = (PotionMeta) im;
					Iterator<PotionEffect> iter = pm.getCustomEffects().iterator();
					PotionEffect next = iter.next();
					sb.append(" effect:" + next.getType().getName() + "," + (next.getDuration() / 20) + "," + next.getAmplifier());
					while (iter.hasNext()) {
						next = iter.next();
						sb.append(" effect:" + next.getType().getName() + "," + (next.getDuration() / 20) + "," + next.getAmplifier());
					}
				}

				else if (im instanceof SkullMeta) {
					sb.append("player:" + ((SkullMeta) im).getOwner());
				}
			}
		} catch (Exception e) {
			System.err.println("\u001B[30;1m[\u001B[32;1manything.spigotmc\u001B[30;1m] \u001B[31;1mCannot serialise ItemStack: " + is.toString() + " - Mabye this is the reason: " + e.toString() + "\u001B[0m");
			return null;
		}
		return sb.toString();
	}

	// ENCHANTMENT

	/**
	 * This is an internal method to the ItemUtils class to allow an enchantment to be retrieved from a string either of its correct name 
	 * or of its more commonly used name (e.g. "PROTECTION" can be used as the string instead of "PROTECTION_ENVIRONMENTAL"). 
	 * This is to make the inputed strings easier for end uses to understand as they are the more common names for the enchantment.
	 * 
	 * @param str
	 *            The string value of the enchantment (its name).
	 * @return The enchantment associated with the provided string if one is found or null if it is not found.
	 */
	private Enchantment getEnchantmentType(String str) {
		str = str.toUpperCase();
		Enchantment en = Enchantment.getByName(str);
		if (en == null) {
			switch (str) {
			case "PROTECTION":
				en = Enchantment.PROTECTION_ENVIRONMENTAL;
				break;
			case "FIRE_PROTECTION":
				en = Enchantment.PROTECTION_FIRE;
				break;
			case "FEATHER_FALLING":
				en = Enchantment.PROTECTION_FALL;
				break;
			case "BLAST_PROTECTION":
				en = Enchantment.PROTECTION_EXPLOSIONS;
				break;
			case "PROJECTILE_PROTCETION":
				en = Enchantment.PROTECTION_PROJECTILE;
				break;
			case "RESPIRATION":
				en = Enchantment.OXYGEN;
				break;
			case "AQUA_AFFINITY":
				en = Enchantment.WATER_WORKER;
				break;
			case "SHARPNESS":
				en = Enchantment.DAMAGE_ALL;
				break;
			case "SMITE":
				en = Enchantment.DAMAGE_UNDEAD;
				break;
			case "BANE_OF_ARTHROPODS":
				en = Enchantment.DAMAGE_ARTHROPODS;
				break;
			case "LOOTING":
				en = Enchantment.LOOT_BONUS_MOBS;
				break;
			case "EFFICIENCY":
				en = Enchantment.DIG_SPEED;
				break;
			case "UNBREAKING":
				en = Enchantment.DURABILITY;
				break;
			case "FORTUNE":
				en = Enchantment.LOOT_BONUS_BLOCKS;
				break;
			case "POWER":
				en = Enchantment.ARROW_DAMAGE;
				break;
			case "PUNCH":
				en = Enchantment.ARROW_KNOCKBACK;
				break;
			case "FLAME":
				en = Enchantment.ARROW_FIRE;
				break;
			case "INFINITY":
				en = Enchantment.ARROW_INFINITE;
				break;
			case "LUCK_OF_THE_SEA":
				en = Enchantment.LUCK;
				break;
			}
		}
		return en;
	}

	/**
	 * This is an internal method to the ItemUtils class to allow an enchantment type to be converted to a string for storage purposes.
	 * 
	 * @param enc
	 *            The enchantment type that is to be serialised. Several else if blocks are used in order to allow a more human readable string to be returned.
	 * @return The string value (name) of the enchantment.
	 */
	private String serialiseEnchantmentType(Enchantment enc) {
		String en = null;
		// Armour enchantment's
		if (enc == Enchantment.PROTECTION_ENVIRONMENTAL) {
			en = "PROTECTION";
		} else if (enc == Enchantment.PROTECTION_FIRE) {
			en = "FIRE_PROTECTION";
		} else if (enc == Enchantment.PROTECTION_FALL) {
			en = "FEATHER_FALLING";
		} else if (enc == Enchantment.PROTECTION_EXPLOSIONS) {
			en = "BLAST_PROTECTION";
		} else if (enc == Enchantment.PROTECTION_PROJECTILE) {
			en = "PROJECTILE_PROTCETION";
		} else if (enc == Enchantment.OXYGEN) {
			en = "RESPIRATION";
		} else if (enc == Enchantment.WATER_WORKER) {
			en = "AQUA_AFFINITY";
		} else if (enc == Enchantment.THORNS) {
			en = "THORNS";
		} else if (enc == Enchantment.DEPTH_STRIDER) {
			en = "DEPTH_STRIDER";
		}
		// Sword enchantment's
		else if (enc == Enchantment.DAMAGE_ALL) {
			en = "SHARPNESS";
		} else if (enc == Enchantment.DAMAGE_UNDEAD) {
			en = "SMITE";
		} else if (enc == Enchantment.DAMAGE_ARTHROPODS) {
			en = "BANE_OF_ARTHROPODS";
		} else if (enc == Enchantment.KNOCKBACK) {
			en = "KNOCKBACK";
		} else if (enc == Enchantment.FIRE_ASPECT) {
			en = "FIRE_ASPECT";
		} else if (enc == Enchantment.LOOT_BONUS_MOBS) {
			en = "LOOTING";
		}
		// Tool enchantment's
		else if (enc == Enchantment.DIG_SPEED) {
			en = "EFFICIENCY";
		} else if (enc == Enchantment.SILK_TOUCH) {
			en = "SILK_TOUCH ";
		} else if (enc == Enchantment.DURABILITY) {
			en = "UNBREAKING";
		} else if (enc == Enchantment.LOOT_BONUS_BLOCKS) {
			en = "FORTUNE";
		}
		// Bow enchantment's
		else if (enc == Enchantment.ARROW_DAMAGE) {
			en = "POWER";
		} else if (enc == Enchantment.ARROW_KNOCKBACK) {
			en = "PUNCH";
		} else if (enc == Enchantment.ARROW_FIRE) {
			en = "FLAME";
		} else if (enc == Enchantment.ARROW_INFINITE) {
			en = "INFINITY";
		}
		// Fishing rod enchantment's
		else if (enc == Enchantment.LUCK) {
			en = "LUCK_OF_THE_SEA";
		} else if (enc == Enchantment.LURE) {
			en = "LURE";
		}

		if (en == null) {
			en = enc.getName();
		}

		return en;
	}
}
