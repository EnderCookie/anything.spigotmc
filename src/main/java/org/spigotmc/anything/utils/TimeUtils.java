package org.spigotmc.anything.utils;

public class TimeUtils {

	/**
	 * A simple method to format a integer value of seconds into a human readable form. 
	 * This can be used with things such as a long system time provided it is first cast to an int. 
	 * This could be useful for remaining time in a cooldown.
	 * 
	 * @param seconds
	 *            The input number of seconds to be formatted
	 * @return A human readable string of the time with days, hours, minutes and seconds where applicable.
	 */
	public static String getFormatedIntTime(int seconds) {
		int minutes = seconds / 60;
		int hours = minutes / 60;
		int days = hours / 24;

		seconds -= minutes * 60;
		minutes -= hours * 60;
		hours -= days * 24;

		StringBuilder sb = new StringBuilder();
		if (days > 0)
			sb.append(days).append(" days ");
		if (hours > 0)
			sb.append(hours).append(" hours ");
		if (minutes > 0)
			sb.append(minutes).append(" minutes ");
		if (seconds > 0)
			sb.append(seconds).append(" seconds ");

		return sb.toString().substring(0, sb.toString().length() - 1);
	}
}
