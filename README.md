![anything.spigotmc](http://i.imgur.com/Nz1e5qG.png)

Δ.spigotmc is a Java Spigot API library based off [Δ.js](https://github.com/Rabrennie/anything.js/) that accepts any Pull Request as long as it follows the rules.

# What??
Basically the community can work together to make a massive library / API / plugin, which no one will probably ever use

# Why
Because [Δ.js](https://github.com/Rabrennie/anything.js/) is hilarious and Spigot deserves one too.

# Rules
1. Nothing that will crash a Spigot server
1. Nothing malicious
1. No removing other's work in PRs
1. Nothing that violates BitBucket's Terms of Service
1. You may not publish other's work without their explicit permission

# Compiling?
1. Make sure you have Δ.spigotmc locally on your computer.
1. Navigate to the base folder (which contains the pom.xml).
1. Open a console / terminal in that folder.
1. Type `mvn clean compile assembly:single`.
1. Compiled jar will be in `./target/`.

# Contributing
1. Fork ( https://bitbucket.org/insou/anything.spigotmc/fork )
1. Clone ( git clone https://bitbucket.org/YourUsername/anything.spigotmc.git )
1. Branch ( git checkout -b your-feature )
1. Make a new package inside org.spigotmc.anything with the function's name
1. Make your code
1. Push ( git push -u origin your-feature )
1. [Pull Request](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html)

We would also like your Pull Request to adhere to the following guidelines:

# Contributing Guidelines

"We will still merge your pull request if it doesn't follow the guidelines, although deep in our soul we'll hate you."

1. Make sure your master branch is up-to-date before creating a branch to develop your feature. See [this resource](https://blog.bitbucket.org/2013/02/04/syncing-and-merging-come-to-bitbucket/) for a guide on how to update your local branch.
1. 4 spaces / tab indentation.
1. Keep open braces on the same line as a method, like such: `public void foo(String bar) {`, same goes for if, switch, etc.
1. We are using Java 8. Deal with it.
1. Maven guidelines:
    1. All external dependencies will be handled with Maven.
    1. You may add any repository you require for a dependency.
    1. You may add any dependency you require for your feature.
    1. You must add a scope to every dependency.
        1. You use `<scope>provided</scope>` if you don't want your dependency compiled inside the jar.
        1. You use `<scope>compile</scope>` if you want your dependency compiled inside the jar.
    1. We are using maven-assembly-plugin over maven-shade-plugin to compile dependencies inside the jar.
        1. All you need to do to have your dependency compiled or not inside the jar is add a scope. That's all.
        1. If you wish to complain about assembly-plugin and why we should use shade, don't.
    1. Suggested Maven arguments: `clean compile assembly:single`.
1. If you need an extra resource - eg. config.yml, <feature-name>.yml, add it in the resources folder and include it in the pom.xml build.
1. Make sure your feature actually works before making a Pull Request.

# Examples / Ideas
1. ~~Fly~~
1. Skin Changer
1. Player Wrapper
1. Stacker Game
1. ...add more ideas here!